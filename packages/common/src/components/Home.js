import React from "react";
import { View, Text, Dimensions } from "react-native";

export const Home = ({ name, color }) => {
  let screenHeight = Dimensions.get("window").height;

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: color || "teal",
        height: screenHeight,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Text style={{ fontSize: 36, color: "white" }}>{name}</Text>
    </View>
  );
};
